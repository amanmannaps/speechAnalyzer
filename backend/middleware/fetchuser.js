const jwt = require('jsonwebtoken');
require('dotenv').config({ path: '../.env' });

const fetchuser = (req,res,next)=>{
    //Get the user form the jwt token and add it to the req object
    
    try {
        const token=req.header('auth-token');
        if(!token){
            console.log("No token");
            return res.status(401).send({error: "Please authenticate using a valid token"});
        }
        const data =jwt.verify(token,`${process.env.JWT_SECRET}`);
        req.user=data.user;
        next();
        
    } catch (error) {
        res.status(401).send({error: "Please authenticate using a valid token"});
    }
    
}

module.exports = fetchuser;