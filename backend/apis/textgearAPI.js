const textgears=require('textgears-api');
require('dotenv').config({path:'../.env'});



const getCorrectGrammer=async (text)=>{
    const textgearsApi = textgears(`${process.env.TEXTGEAR_API_KEY}`, {language: 'en-US', ai: true});
    let grammerPromise=new Promise((resolve,reject)=>{
        textgearsApi.correct(text).then((data)=>{
            resolve(data.response.corrected);
        }).catch((err)=>{
            reject(err);
        })
    })
    return grammerPromise;
}
// console.log(getCorrectGrammer("I is an engineeeer"));

module.exports=getCorrectGrammer;
