const speech = require('@google-cloud/speech');

let speechApi = async (audioBuffer) => {
    const client = new speech.SpeechClient();

    const request = {
        config: {
            languageCode: 'en-IN',
            enableWordTimeOffsets: true,
            enableAutomaticPunctuation: true,
        },
        audio: {
            content: audioBuffer.toString('base64')
        }
    };

    // const [response] = await client.recognize(request);
    return client.recognize(request);
}

module.exports = speechApi;