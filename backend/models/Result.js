const mongoose=require('mongoose');

let resultSchema= new mongoose.Schema({
    user:{
        type:mongoose.Schema.Types.ObjectId ,
        ref:'user'
    },
    speech:{
        type:  mongoose.Schema.Types.ObjectId ,
        ref:'speech'
    },
    transcribe:{
        type:String,
        required:true
    },
    correctGrammer:{
        type:String,
        required:true
    },
    fluency:{
        type:Number,
        required:true
    },
    detailedTranscibe:{
        type: String,
        required :true
    },
    segmentedResult:{
        type:Array,
        required:true
    },
    mostUsedWords:{
        type:Array,
        required:true
    },
    leastUsedWords:{
        type:Array,
        required:true
    },
    difficultyArray:{
        type:Array,
        required:true
    },
    overallScore:{
        type:Number,
        required:true
    },
    fluencyScore:{
        type:Number,
        required:true
    },
    grammerScore:{
        type:Number,
        required:true
    },
    advanceVocublary:{
        type:Number,
        required:true
    },
    freqObject:{
        type:mongoose.Schema.Types.Mixed,
        required:true
    },
    wordCount:{
        type:Number,
        required:true
    }
})

let Result=mongoose.model('result',resultSchema);
module.exports=Result;