const speechApi = require('../apis/googleTranscribeApi');
// const getCorrectGrammer = require('../apis/textgearAPI');
// const postSpeechObj = require('./postSpeechObj');

// let wordCount = 0;
// let transcribe = "";
// let detailedTranscibe = "";


let getResponse =async (speechBuffer,data) => {
    data.wordCount=0;
    data.transcribe="";
    data.detailedTranscibe="";
    const [response] = await speechApi(speechBuffer);

    response.results.forEach((result) => {
        result.alternatives.forEach((alternative) => {
            // console.log(alternative.transcript);
            data.transcribe += alternative.transcript ;
            data.wordCount += alternative.words.length;
            // console.log(alternative.words.length);
            alternative.words.forEach((item) => {
                // console.log(`  ${item.word}`);
                const s = parseInt(item.startTime.seconds) + item.startTime.nanos / 1000000000;
                // console.log(`   WordStartTime: ${s}s`);
                const e = parseInt(item.endTime.seconds) + item.endTime.nanos / 1000000000;
                // console.log(`   WordEndTime: ${e}s`);
                data.detailedTranscibe += item.word + " " + s + " " + e + " ";
            });
        });
    });

}


let getFluency = (data) => {
    if (data.wordCount == 0) {
        return 0;
    }
    let transcribeArray = data.detailedTranscibe.split(" ");
    let startTime = parseFloat(transcribeArray[1]);
    let endTime = parseFloat(transcribeArray[transcribeArray.length - 1]==''?transcribeArray[transcribeArray.length - 2]:transcribeArray[transcribeArray.length - 1]);
    let totalTime = endTime - startTime;
    return Math.round((data.wordCount*60) / totalTime);
}

// let correctGrammer = async (transcribe) => {
//     return getCorrectGrammer(transcribe);

// }

// (async () => { console.log(await correctGrammer("I is an engineeer")) })();

let getSegmentedTranscribe = (detailedTranscibe) => {
    let transcribeArray = detailedTranscibe.split(" ");
    let audioStartTime = parseFloat(transcribeArray[1]);
    let segmentedResult = [];
    let currentStr = "";
    let nextTimeStop = 15;
    for (let i = 0; i < transcribeArray.length; i += 3) {
        let word = transcribeArray[i];
        let startTime = parseFloat(transcribeArray[i + 1]) - audioStartTime;
        let endTime = parseFloat(transcribeArray[i + 2]) - audioStartTime;
        if (startTime >= nextTimeStop) {
            segmentedResult.push(currentStr);
            nextTimeStop += 15;
            currentStr = word;
            continue;
        }
        else if (endTime >= nextTimeStop) {
            currentStr += word;
            segmentedResult.push(currentStr);
            nextTimeStop += 15;
            currentStr = "";
            continue;
        }
        else {
            currentStr += word+" ";
        }
    }
    if (currentStr != "") {
        segmentedResult.push(currentStr);
    }
    return segmentedResult;
}



module.exports={getResponse,getFluency,getSegmentedTranscribe};