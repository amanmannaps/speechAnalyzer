const express=require('express');
const Result=require('../models/Result');
const Speech=require('../models/Speech');
const User=require('../models/User');
const fetchuser=require('../middleware/fetchuser');
require('dotenv').config({path:'../.env'});
const bodyParser = require('body-parser');
const jsonParser=bodyParser.json();
const axios=require('axios');
const {getResponse,getFluency,getSegmentedTranscribe}=require('../result/generateReport');
// const textgears=require('../apis/textgearAPI');
const textgears=require('textgears-api');
let natural=require('natural');
let {create}=require('difficulty');
const sm=require('string-mismatch');
const greedyInstance = new sm.Greedy();

const router=express.Router();
const TEXTGEAR_API_KEY='dX9zw6f0Mp4ASv8p'
// Route 1- Get all the results of the logged in user

router.get('/fetchallresults',fetchuser,async (req,res)=>{
    try {
        let allResult=await Result.find({user:req.user.id});
        res.send(allResult);
    } 
    catch (error) {
        console.error(error.message);
        res.status(500).send("Internal Server Error");
    }
})

// Route 2- Get a particular result of the logged in user

router.get('/getresult/:id',fetchuser,async (req,res)=>{
    try {
        let {id}=req.params;
        let result=await Result.findById(id);
        // If the logged in user is not same as the user of the result 
        if(result.user.toString()!=req.user.id){
            return res.status(401).send("Not allowed");
        }
        // console.log(result.user)
        res.send(result);
    } 
    catch (error) {
        console.error(error.message);
        res.status(500).send("Internal Server Error");
    }
})


// Route 3- Post request to upload the result to the database

router.post('/createresult',fetchuser,async (req,res)=>{
    // try {
        // let {transcribe,correctGrammer,fluency,detailedTranscibe,segmentedResult}=req.body;
        // Getting the speechId from the header
        let speechId=req.header('speech-id');
        let userId=req.user.id;
        let user=await User.findById(userId);
        let speech=await Speech.findById(speechId);
        // console.log(req.header('auth-token'));
        const speechResponse = await axios.get(`http://localhost:5000/speech/getspeech/${speechId}`, {
                headers: {
                    // 'Content-Type': 'multipart/form-data',
                    // 'Content-Type': 'application/json'
                    'auth-token':req.header('auth-token')
                }
            });
        // const speech=await Speech.findById(speechId);
        // let buffer=Buffer.from(speech.audio.buffer);
        // console.log(buffer);

        // // console.log(speechResponse);
        let data={};
        
        // console.log(speechResponse.data.audio.buffer);

        await getResponse(speechResponse.data.audio.buffer,data);
        // // console.log(data);

        let fluency=getFluency(data);
        let segmentedResult=getSegmentedTranscribe(data.detailedTranscibe);


        const textgearsApi = textgears(`${TEXTGEAR_API_KEY}`, {language: 'en-US', ai: true});
        
        let correctGrammer="";
        if(data.wordCount!=0){
            let correctGrammerResponse=await textgearsApi.correct(data.transcribe);
            correctGrammer=correctGrammerResponse.response.corrected;
        }


        // sorting the words based on the frequency
        
        let tokenizer = new natural.WordTokenizer();
        let tokens = tokenizer.tokenize(data.transcribe);

        // Grouping the words based on difficulty
        const difficulty=await create();
        let difficultyArray=[[],[],[],[]];
        
        let freqObject = {};
        for (let token of tokens) {
            token=token.toLowerCase();
            let firstLetterCapital=token.charAt(0).toUpperCase()+token.slice(1);
            freqObject[firstLetterCapital] = (freqObject[firstLetterCapital] || 0) + 1;
            let difficultyLevel=Math.min(difficulty.getLevel(token),difficulty.getLevel(firstLetterCapital));
            difficultyArray[difficultyLevel].push(firstLetterCapital);
        }
        difficultyArray[0]=Array.from(new Set(difficultyArray[0]));
        difficultyArray[1]=Array.from(new Set(difficultyArray[1]));
        difficultyArray[2]=Array.from(new Set(difficultyArray[2]));
        difficultyArray[3]=Array.from(new Set(difficultyArray[3]));

        // console.log(difficultyArray[0]);
        // console.log(difficultyArray[1]);
        // console.log(difficultyArray[2]);
        // console.log(difficultyArray[3]);

        let sortedWords = Object.keys(freqObject).sort((a, b) => freqObject[b] - freqObject[a]);
        //   console.log(sortedWords);
        let mostUsedWords=[];
        let leastUsedWords=[];
        let mostUsedWordFreq=0;
        if(freqObject[sortedWords[0]]>1){
            mostUsedWordFreq=freqObject[sortedWords[0]];
        }

        for(let word of sortedWords){
            if(freqObject[word]==mostUsedWordFreq){
                mostUsedWords.push(word);
            }
            else if(freqObject[word]==1){
                leastUsedWords.push(word);
            }
        }
        
        // console.log(data);

        // console.log(correctGrammer.response.corrected);
        // console.log(data,fluency,segmentedResult);
        // console.log(speechResponse.data)
        // let overallScore=70;
        // let grammerScore=80;
        // let fluencyScore=
        // let advanceVocublary=10;
        let fluencyScore= Math.round(((Math.abs(fluency-150))*100)/150);
        // let grammerScore=
        let ins=0,del=0;
        greedyInstance.differences(data.transcribe,correctGrammer).forEach((item)=>{
            if(item.type=="del"){
                del++;
            }
            else if(item.type=='ins'){
                ins++;
            }
        });

        let grammerScore=Math.round( ((Math.min(ins,del)+Math.abs(ins-del)) *100) /(data.wordCount));

        let overallScore=(grammerScore+fluencyScore)/2;
        let advanceVocublary=10;

        let result=new Result({
            transcribe:data.transcribe,
            correctGrammer,
            fluency,
            detailedTranscibe:data.detailedTranscibe,
            speech:speechResponse.data,
            segmentedResult,
            mostUsedWords,
            leastUsedWords,
            difficultyArray,
            speech,
            user,
            overallScore:(100-overallScore),
            grammerScore:(100-grammerScore),
            fluencyScore:(100-fluencyScore),
            advanceVocublary,
            freqObject,
            wordCount:data.wordCount
        })

        // let result = new Result({
        //     transcribe,correctGrammer,fluency,detailedTranscibe,speech:speechId,user:userId,segmentedResult
        // })
        let savedResult = await result.save();
        res.json(savedResult);
    // } 
    // catch (error) {
    //     console.log(error);
    //     console.error(error.message);
    //     res.status(500).send("Internal Server Error");
    // }
})

module.exports=router;

