const express = require('express');
const Speech = require('../models/Speech');
const User = require('../models/User');
const fetchuser = require('../middleware/fetchuser');
const bodyParser = require('body-parser');
const multer = require('multer');
require('dotenv').config({ path: '../.env' });

const storage = multer.memoryStorage();
const upload = multer({ storage });

const router = express.Router();

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());


// Route 1 -> Get a single speech from the provided id

router.get('/getspeech/:speechid',fetchuser,async (req,res)=>{
    try {
        const speechid=req.params.speechid;
        // console.log(speechid);
        const speech=await Speech.findById(speechid);
        
        // // Check if the speech exists or not
        if(!speech){
            return res.status(404).send("Speech Not Found");
        }

        // Check if the speech has the same user as the logged in user
        if(speech.user.toString() != req.user.id){
            return res.status(401).send("Not Allowed");
        }
        res.send(speech);
    } 

    catch (error) {
        console.error(error.message);
        res.status(500).send("Internal Server Error");
    }
})


// Route 2-> Post request to upload the recorded audio for the logged in user

router.post('/addspeech', fetchuser, upload.single('audio'), async (req, res) => {
    try {
        const speechBlob = req.file;
        // const reader = new FileReader();
        // reader.readAsArrayBuffer(speechBlob);
        // const user = Speech.findById(req.user.id);
        const user=await User.findById(req.user.id);
        // Check if the user actually exists
        if(!user){
            return res.status(404).send("User Not Found");
        }

        // const data = {};

        // Used to upload the received speech to the DB
        // reader.onloadend = async function () {
        //     // const buffer = new Buffer(reader.result);
            
        // }
        const speech = new Speech({
            audio: speechBlob,
            user: user,
            audioURL:req.body.audioURL
        })
        let data = await speech.save();
        res.send(data);
    } 
    
    catch (error) {
        console.error(error.message);
        res.status(500).send("Internal Server Error");
    }
})

module.exports = router;

