import React from 'react'
import { Link } from 'react-router-dom'
import CircularChart from './CircularChart'

export default function ResultCard(props) {

    return (
        <>
            <div className="card d-flex flex-row my-3 justify-content-between">
                <div className='d-flex flex-row'>
                    <div className='d-flex align-items-center'>
                        <CircularChart score={props.item.overallScore}></CircularChart>
                    </div>
                    <div className='d-flex align-items-center mx-4'><h3>Overall Score : {props.item.overallScore}</h3></div>
                </div>
                <div className="card-body">
                    <h5 className="card-title">Your text</h5>
                    <p className="card-text">{props.item.transcribe}</p>
                    <Link to={`/singleresult/${props.item._id}`} className="btn btn-primary">More details</Link>
                </div>
            </div>
        </>
    )
}
