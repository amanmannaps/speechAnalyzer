import React, { useState, useEffect, useRef } from 'react'
import MicOff from './MicOff'
import MicOn from './MicOn'
// import axios from 'axios'
// import speechApi from '../javascript/apis/googleTranscribeApi'
// import speech from '@google-cloud/speech';
// import getCorrectGrammer from '../javascript/apis/textgearApi'
import postResult from '../javascript/postRequest/postRequest'
import Login from '../pages/Login'
import Signup from '../pages/Signup'
import { useNavigate } from 'react-router'
import Loading from './Loading'

// console.log(getResponse);
// console.log(getCorrectGrammer);
// console.log(process.env.TEXTGEAR_API_KEY);

export default function Recording() {
    const [recordingOn, setRecordingOn] = useState(false)
    const [stream, setStream] = useState(null)
    const [recorder, setRecorder] = useState(null)
    const [blob, setBlob] = useState(null)
    const [login, setLogin] = useState(false)
    const [signup, setSignup] = useState(false)
    const [token, setToken] = useState("");
    const [click, setClick] = useState(false)
    const buttonRef = useRef(null);
    const [speechResponse, setSpeechResponse] = useState(null)
    const [resultResponse, setResultResponse] = useState(null)
    const [loading, setLoading] = useState(false);

    let navigate = useNavigate();
    useEffect(() => {
        if (token != "") {
            setLogin(false);
            setSignup(false);
            if (click) {
                buttonRef.current.click();
                // console.log("Hey");
            }
        }
    }, [token])

    useEffect(() => {
        if (resultResponse != undefined && resultResponse) {
            setLoading(false);
            navigate(`/singleresult/${resultResponse._id}`);
        }
    }, [resultResponse])

    let handleStartOnClick = async () => {
        setRecordingOn(true);
        let stream = await navigator.mediaDevices.getUserMedia({ audio: true });
        let recorder = new MediaRecorder(stream);
        setStream(stream);
        setRecorder(recorder);

        recorder.start();
        recorder.ondataavailable = async (e) => {
            // console.log(e.data);
            // data.push(e.data);
            setBlob(e.data);
            // setUrl(URL.createObjectURL(blob));
            // console.log(typeof (e.data));
            // console.log(e.data.size);
            // const audioUrl = URL.createObjectURL(e.data);
            // console.log(audioUrl);
            // setAudio(audioUrl);
            // console.log(e.data);
            // const arrayBuffer=await e.data.arrayBuffer();
            // const buffer=Buffer.from(arrayBuffer);
            // console.log(buffer);
            // const formData = new FormData();
            // formData.append('audio', e.data);
            // formData.append('audioURL', audioUrl);
            // let postdata={
            //     audio:buffer.toString('base64'),
            //     audioURL:audioUrl
            // }
            // console.log(localStorage.getItem('token'));
            // const speechResponse = await axios.post('http://localhost:5000/speech/addspeech', formData, {
            //     headers: {
            //         'Content-Type': 'multipart/form-data',
            //         // 'Content-Type': 'application/json'
            //         'auth-token':localStorage.getItem('token')
            //     }
            // });
            // // console.log(response.data.audio.buffer);
            // console.log(speechResponse);



            // let data={

            // }

            // const resultResponse = await axios.post('http://localhost:5000/result/createresult', formData, {
            //     headers: {
            //         'Content-Type': 'multipart/form-data',
            //         // 'Content-Type': 'application/json'
            //         'auth-token':localStorage.getItem('token')
            //     }
            // });

        }

    }

    let handleStopOnClick = () => {
        setRecordingOn(false);
        recorder.stop();
        stream.getTracks().forEach(track => track.stop());
        setStream(null);
    }

    let handleGetResult = async () => {
        // console.log(localStorage.getItem('token'));
        if (localStorage.getItem('token')) {
            setLoading(true);
            let token = localStorage.getItem('token');
            await postResult(blob, token, setResultResponse, setSpeechResponse);

        }
        else {
            setLogin(true);
            setClick(true);
        }
    }

    let handleSwitchClick = () => {
        setLogin(!login);
        setSignup(!signup);
    }

    // let handleGetAudio = async () => {
    //     try {
    //         const response = await axios.get('http://localhost:5000/');
    //         console.log(response.data.audio.buffer.toString('base64'));  
    //     }
    //     catch (err) {
    //         console.log(err);
    //     }
    // }

    // let handleAudioPlay = async () => {
    //     let playPromise=await audioRef.current.play();
    //     console.log(typeof(playPromise));
    //     console.log(playPromise);
    // }

    // let testFunction= async ()=>{
    //     let result = await getCorrectGrammer('I is an engeneer','dX9zw6f0Mp4ASv8p');
    //     console.log(result)
    // }


    return (
        <>

            {!loading? <>
                <div className='d-flex justify-content-center'>
                    {recordingOn ? <MicOn></MicOn> : <MicOff></MicOff>}
                </div>
                <div className='d-flex justify-content-center'>
                    <button type="button" className="btn btn-primary mx-1 my-3" disabled={recordingOn} onClick={handleStartOnClick}>Start Recording</button>
                    <button type="button" className="btn btn-secondary mx-1 my-3" disabled={!recordingOn} onClick={handleStopOnClick}>Stop Recording</button>
                </div>
                <div className='d-flex justify-content-center my-3'>
                    {blob && <button ref={buttonRef} className="btn btn-primary mx-1 my-3" onClick={handleGetResult}>Get Result</button>}
                </div>
                <div className='d-flex justify-content-center'>
                    {(login || signup) && <button type='button' className="btn btn-primary mx-1 my-3" onClick={handleSwitchClick}> {login ? "Create an account" : "Login to existing account"}</button>}
                </div>
                {/* <button type='button' className="btn btn-secondary mx-1 my-3" onClick={testFunction}>Test</button> */}
                {login && <Login cameFrom="postResult" setToken={setToken} />}
                {signup && <Signup cameFrom="postResult" setToken={setToken} />}
            </>:
            <Loading/>
            }
        </>
    )
}
