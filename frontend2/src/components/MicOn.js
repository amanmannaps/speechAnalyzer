import React from 'react'
import micon from './pictures/micon.gif'

export default function MicOn() {
  return (
    <div className='text-center my-3'>
        <img src={micon} alt="Microphone On" height='70px'></img>
    </div>
  )
}
