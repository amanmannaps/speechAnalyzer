import React from "react";
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
} from "chart.js";
import { Bar } from "react-chartjs-2";

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
);


export default function BarChart(props) {

    let dataValues=[];

    let {difficultyArray,freqObject,wordCount}=props.resultObj;

    difficultyArray.forEach((subarr)=>{
        let sum=0;
        subarr.forEach((item)=>{
            sum+=freqObject[item];
        })
        dataValues.push(Math.round((sum*100)/wordCount));
    })

    const options = {
        responsive: true,
        indexAxis: 'y',
        plugins: {
            legend: {
                display: false
            },
            title: {
                display: true,
                text: "Chart based on vocabulary used",
                position: "bottom"
            },
        },
        scales: {
            x:
            {
                min: 0,
                max: 100,
            },
        }


    };
    // const labels = ["Level 0", "Level 1", "Level 2", "Level 3"]

    const data = {
        labels: ["Level 3", "Level 2", "Level 1", "Level 0"],
        datasets: [
            {
                
                data: dataValues.reverse(),
                backgroundColor: "rgba(255, 99, 132, 0.5)"
            }
        ]
    };

    return (
        <div>
            <Bar options={options} data={data} />
        </div>
    )
}
