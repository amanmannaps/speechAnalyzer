import React from 'react'
import CircularChart from './CircularChart'

export default function Grammer(props) {
  return (
    <div className='d-flex flex-column my-2'>
      <div className='mb-3'>
        <div className='d-flex flex-row'>
          <CircularChart score={props.resultObj.grammerScore}></CircularChart>
          <div className='d-flex align-items-center mx-4'><h3>Score : {props.resultObj.grammerScore}%</h3></div>
        </div>
      </div>
      <div className='d-flex flex-row my-2'>
        <div className='d-flex flex-column mx-2 p-3 bg-danger bg-opacity-10 border border-danger border-start-0 rounded-end'>
          <h5>Correct Grammer</h5>
          <div>{props.resultObj.correctGrammer}</div>
        </div>
        <div className='d-flex flex-column mx-2 p-3 bg-danger bg-opacity-10 border border-danger border-start-0 rounded-end'>
          <h5>Your text</h5>
          <div>{props.resultObj.transcribe}</div>
        </div>
      </div>
    </div>
  )
}
