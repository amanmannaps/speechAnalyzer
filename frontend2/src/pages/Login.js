import React,{useState} from 'react'
import { useNavigate } from 'react-router-dom';

export default function Login(props) {
    const [credentials, setCredentials] = useState({ email: "", password: ""});
    let navigate=useNavigate();
    const handleSubmit=async (e)=>{
        e.preventDefault();
        const response = await fetch(`http://localhost:5000/user/login`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({email:credentials.email,password:credentials.password})
          });
          const json = await response.json();
        if(json.success){
            // Save the auth token and redirect
            localStorage.setItem('token',json.authToken);
            console.log(json);
            if(!props.cameFrom){
                navigate('/');
            }
            else{
                props.setToken(json.authToken);
            }
            // props.showAlert("Sign-Up Successful","success");
        }
        else{
            // props.showAlert("Invalid Credential","danger");
            console.log("Please enter correct details")
        }
    }

    const onChange=(e)=>{
        setCredentials({...credentials,[e.target.name]:e.target.value})
    }
  return (
    
    <div className='container mt-2'>
            <h2 className='mt-2 mb-4'>Login to use SpeechAnalyzer</h2>
            <form onSubmit={handleSubmit}>
                <div className="form-floating mb-3">
                    <input type="email" className="form-control" id="email" onChange={onChange} name='email' aria-describedby="emailHelp" minLength={5} placeholder="name@example.com" />
                    <label htmlFor="email" className="form-label">Email address</label>
                </div>
                <div className="form-floating mb-3">
                    <input type="password" className="form-control" onChange={onChange} name='password' id="password" minLength={5}  placeholder="Password@123"/>
                    <label htmlFor="password" className="form-label">Password</label>
                </div>
                <div className="d-flex justify-content-between">
                    <button type="submit" className="btn btn-primary">Submit</button>
                </div>
                
            </form>
        </div>
  )
}
