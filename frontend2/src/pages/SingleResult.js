import axios from 'axios'
import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router';
import { Buffer } from 'buffer';
import Overview from '../components/Overview';
import Fluency from '../components/Fluency';
import Grammer from '../components/Grammer';
import Vocabulary from '../components/Vocabulary';

export default function SingleResult(props) {
    let { resultID } = useParams();
    const [resultObj, setResultObj] = useState(null);
    const [audioURL, setAudioURL] = useState(null);
    const [audio, setAudio] = useState(null);
    const borderStyle={'border':'solid 2px red','padding':'10px'};
    // console.log(resultObj);
    useEffect(() => {
        if (audioURL) {
            setAudio(new Audio(audioURL));
            // console.log(resultObj);
        }
    }, [audioURL])


    useEffect(() => {
        const fetchCall = async () => {
            const speechResponse = await axios.get(`http://localhost:5000/speech/getspeech/${resultObj.speech.toString()}`, {
                headers: {
                    'auth-token': localStorage.getItem('token')
                }
            })
            // setAudioURL(speechResponse.data.audioURL);
            // console.log();
            let buffer = Buffer.from(`${speechResponse.data.audio.buffer}`, 'base64');
            let blob = new Blob([buffer], { type: 'audio/mp3' });
            const audioUrl = URL.createObjectURL(blob);
            setAudioURL(audioUrl);
        }
        if (resultObj != {} && resultObj != null && resultObj != undefined) {
            fetchCall();
        }
    }, [resultObj])
    useEffect(() => {
        const fetchCall = async () => {
            const resultResponse = await axios.get(`http://localhost:5000/result/getresult/${resultID}`, {
                headers: {
                    'auth-token': localStorage.getItem('token')
                }
            });
            setResultObj(resultResponse.data);
        }
        fetchCall();
    }, [])
    
    return (
        <div className='d-flex justify-content-center container' style={{}}>
            {resultObj && audioURL && <>
                <div className='my-2' style={{'border':'solid 2px black','padding':'10px'}}>
                    <div className='my-2 '>
                        <h4 className='text-center mb-3' style={{'fontWeight':'700','textDecoration':'underline'}}>Your audio report</h4>
                    </div>
                    <div className='my-2' style={borderStyle}>
                        <h2 className='text-center my-1'>Overview</h2>
                        <Overview resultObj={resultObj} audioURL={audioURL}></Overview>
                    </div>
                    <div className='my-2' style={borderStyle}>
                        <h2 className='text-center my-1'>Fluency</h2>
                        <Fluency resultObj={resultObj}></Fluency>
                    </div>
                    <div className='my-2' style={borderStyle}>
                        <h2 className='text-center my-1'>Grammer</h2>
                        <Grammer resultObj={resultObj}></Grammer>
                    </div>
                    <div className='my-2' style={borderStyle}>
                        <h2 className='text-center my-1'>Vocabulary</h2>
                        <Vocabulary resultObj={resultObj}></Vocabulary>
                    </div>
                </div>
            </>
            }
        </div>
    )
}
