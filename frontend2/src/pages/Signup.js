import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'


export default function Signup(props) {
    const [credentials, setCredentials] = useState({ email: "", password: "", name: "", age: 0, pNumber: 9999999999 });
    const [validDetails, setValidDetails] = useState(true);
    let navigate = useNavigate();
    const handleSubmit = async (e) => {
        e.preventDefault();
        if (password == "" || email == "" || name == "") {
            setValidDetails(false);
            setTimeout(() => {
                setValidDetails(true);
            }, 1500);
            return;
        }
        // navigate('/');
        const response = await fetch(`http://localhost:5000/user/signup`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ email: credentials.email, password: credentials.password, name: credentials.name, age: credentials.age, phoneNumber: credentials.pNumber })
        });
        const json = await response.json();
        if (json.success) {
            // Save the auth token and redirect
            localStorage.setItem('token', json.authToken);
            // console.log(json);
            props.setToken(json.authToken);
            if (!props.cameFrom) {
                navigate('/');
            }
            // props.showAlert("Sign-Up Successful","success");
        }
        else {
            // props.showAlert("Invalid Credential","danger");
            console.log("Please enter correct details")
        }
    }

    const onChange = (e) => {
        setCredentials({ ...credentials, [e.target.name]: e.target.value })
    }
    return (
        
        <>
            <div className='container mt-2'>
                
                <h2 className='my-2' > Create an account to use SpeechAnalyzer</h2 >
                {!validDetails && <h5 className='my-3 text-danger' >!Please enter the valid details</h5 >}

                <form onSubmit={handleSubmit}>
                    <div className="form-floating mb-3">
                        <input type="text" className="form-control" id="name" onChange={onChange} name='name' aria-describedby="emailHelp" minLength={3} placeholder="name"/>
                        <label htmlFor="name" className="form-label">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input type="email" className="form-control" id="email" onChange={onChange} name='email' aria-describedby="emailHelp" minLength={5} placeholder="name@example.com"/>
                        <label htmlFor="email" className="form-label">Email address</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input type="password" className="form-control" onChange={onChange} name='password' id="password" minLength={5} placeholder="password"/>
                        <label htmlFor="password" className="form-label">Password</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input type="number" className="form-control" onChange={onChange} name='pNumber' id="pNumber" min={6000000000} max={9999999999} placeholder={9999999999} />
                        <label htmlFor="pNumber" className="form-label">Phone Number</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input type="number" className="form-control" onChange={onChange} name='age' id="age" min={4} max={100} placeholder={10} />
                        <label htmlFor="age" className="form-label">Age</label>
                    </div>

                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div >

        </>
    )
}
