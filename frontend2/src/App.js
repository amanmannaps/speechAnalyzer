import './App.css';
import Navbar from './components/Navbar';
import Home from './pages/Home';
import Results from './pages/Results';
import Signup from './pages/Signup';
import Login from './pages/Login';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import SingleResult from './pages/SingleResult';

function App() {
  return (
    <>
      <BrowserRouter>
        <Navbar></Navbar>
        <Routes>
          <Route exact path='/' element={<Home/>}></Route>
          <Route exact path='/results' element={<Results/>}></Route>
          <Route exact path='/login' element={<Login/>}></Route>
          <Route exact path='/signup' element={<Signup/>}></Route>
          {/* <Route exact path='/barchart' element={<BarChart/>}></Route> */}
          <Route exact path='/singleresult/:resultID' element={<SingleResult/>}></Route>
          {/* <Route exact path='/test' element={<Test/>}></Route> */}
          
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
