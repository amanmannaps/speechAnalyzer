import axios from 'axios';

let postResult = async (blob, token ,setResultResponse,setSpeechResponse) => {
    const audioUrl = URL.createObjectURL(blob);
    console.log(blob);
    const formData = new FormData();
    formData.append('audio', blob);
    formData.append('audioURL', audioUrl);
    const speechResponse = await axios.post('http://localhost:5000/speech/addspeech', formData, {
        headers: {
            'Content-Type': 'multipart/form-data',
            // 'Content-Type': 'application/json'
            'auth-token': token
        }
    });
    // console.log(speechResponse);
    setSpeechResponse(speechResponse.data);

    const resultResponse= await axios.post('http://localhost:5000/result/createresult',{},{
        headers:{
            'speech-id':speechResponse.data._id,
            'auth-token': token
        }
    })
    // console.log(resultResponse);
    setResultResponse(resultResponse.data);
}

export default postResult;